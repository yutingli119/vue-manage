import Vue from 'vue'
import App from './App.vue'
//register Element UI
import ElementUI from 'element-ui' 
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/css/index.css'
//register vue router & vuex
import router from './router'
import store from './vuex/store'
import axios from "axios"

Vue.prototype.$axios = axios
Vue.config.productionTip = false
Vue.use(ElementUI);

//initialize
init()

function init(){
  
  //do other things there:)

  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
}