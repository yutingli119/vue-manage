import qs from "qs"
import RequestHttp from '@/service/Request'
import RequestUrl from '@/service/RequestUrl'


// define an api here
export const register = (obj) => {
    const data = qs.parse(obj)
    return RequestHttp({
        url: RequestUrl.user.register,
        method: "POST",
        data: data
    });
};
