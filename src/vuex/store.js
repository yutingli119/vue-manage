import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    login:true,
  },
  mutations: {
    toggleShowLogin(state, toggle){
      state.login = !!toggle
    },
  },

  actions: {
    toggleShowLogin({commit}, toggle){
      commit('toggleShowLogin', toggle)
    },
  },
  modules: {
  }
})
