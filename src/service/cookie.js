
export default {
	set: function (name, value, opts){
		console.log('opts',opts)
		opts = opts || {}
		var expires = opts.expires
		var domain = opts.domain
		var path = opts.path || '/'
		var secure = opts.secure ? 'secure' : ''

		let cookie = name + '=' +  encodeURI(value) + ';'
		if( expires || expires === 0 ){
			var oDate = new Date();
			oDate.setDate( oDate.getDate() + expires );
			cookie += ' expires=' + oDate.toUTCString() + ';';
		}

		if(domain)cookie += ' domain=' + domain + ';'
		if(path)cookie += ' path=' + path + ';'
		if(secure)cookie += ' secure;'
		cookie += 'SameSite=None'
		return document.cookie = cookie

	},

	get: function (name){
		var result = '', reg = new RegExp('(^|; )' + name + '=([^;]*)(;|$)');
		if( result = document.cookie.match(reg) ){
			return decodeURI( result[2] );
		}else{
			return '';
		}
	},

	remove: function (name){
		this.set(name, '', {expires: -1});
	}

};
