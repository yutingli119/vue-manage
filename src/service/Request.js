import axios from 'axios'
import Cookie from './cookie.js'
import Vue from 'vue'
import qs from 'qs'
import store from '../vuex/store'

// 创建axios实例
const service = axios.create({
   baseURL: '/', //api domain here
   timeout: 5000,
   withCredentials: true,
   headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
   }
})

service.interceptors.request.use( config => {
    
    if(!config.headers.Authorization){
    //let token = '1111111'  //token for debug use
    //config.headers.Authorization = token  //token 
      config.headers.Authorization = Cookie.get('Authorization') 
    }

    bFinished = false;
    if(!config._noLoading){
        setTimeout(function () {
            if(!bFinished){
                toggleLoading(true)
            }
        }, showLoadingTime);
    }

    //format data send to back end
    if(/application\/x-www-form-urlencoded/.test(config.headers['Content-Type']) && typeof config.data === 'object'){
        config.data = qs.stringify(config.data, { arrayFormat: 'repeat' })
    }

    return config;
}, error => {
    console.log(error);
    if(Vue.$$app){Vue.$$app.$message({ message: msg, type: 'error', duration: 2000 })}
    return Promise.reject(error);
})

// axios response
service.interceptors.response.use(response => {

    response.headers['Set-Cookie'] = 'Secure;SameSite=None'
    
    msg = response.msg ||  response.message || ''

    if(response.statusCode!=200 && msg == 'success' && !response.config._noInterceptor){
        msg = ''
    }
    if(msg == 'Login in expired' || msg == 'Please login' && Vue.$$app){
        store.dispatch('toggleShowLogin', true)
    }

    if(response.statusCode !== 200 && !response.config._noInterceptor){
        if(Vue.$$app)Vue.$$app.$message({ message: msg, type: 'error', duration: 2000 })
    }

     return response;

}, error => {
    // if there is an error, redirect to login page
   store.dispatch('toggleShowLogin', false)    
    return Promise.reject(error);
})

const RequestHttp = async (config) => {
  try {
    const res = await service(config)
    return Promise.resolve(res.data)
  } catch (error) {
    console.log(error)
    return Promise.reject(error)
  }
}

let $vmLoading
function toggleLoading(show) {
    if(show){
        $vmLoading = Vue.$$app && Vue.$$app.$loading({
            lock: true,
            text: 'loading'
        }) 
    }else{
        $vmLoading && $vmLoading.close()
    }
}

export default RequestHttp