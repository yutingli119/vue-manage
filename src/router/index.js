import Vue from 'vue'
import VueRouter from 'vue-router'
import home from '../views/home/index.vue'
import manage from '../views/manage/index.vue'
import Login from '../components/login.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
    {
      path: '/',
      name: 'home',
      component: home,
    },
  {
    path:'/manage',
    name: 'manage',
    component: manage,
  },

]

const router = new VueRouter({
  routes
})

export default router
